## We specify the base image we need for our
## go application
FROM devsecopsacademy/golang:1.15.6-alpine3.12
## We copy everything in the root directory
## into our /go directory
ADD . /go
## We specify that we now wish to execute
## any further commands inside our /go
## directory
RUN cd /go
## we run go build to compile the binary
## executable of our Go program
RUN go build -o main .
## Our start command which kicks off
## our newly created binary executable
CMD ["/go/main"]

EXPOSE 80
EXPOSE 22
